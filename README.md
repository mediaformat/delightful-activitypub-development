# delightful activitypub development [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of resources for ActivityPub developers who create software for the Fediverse.

Emoji's for each entry indicate whether it is added to the [Fediverse Party](https://fediverse.party) website:

* :heavy_check_mark: == added to live website at [**fediverse.party**](https://fediverse.party) (Only set by [@lostinlight](https://codeberg.org/lostinlight))

## Contents

- [Developer tools](#developer-tools)
  - [Frameworks](#frameworks)
  - [Libraries](#libraries)
  - [Plugins](#plugins)
  - [Relays](#relays)
  - [Bridges](#bridges)
  - [Utilities](#utilities)
  - [Testing](#testing)
- [Forge federation](#forge-federation)
  - [Forge libraries](#forge-libraries)
  - [Forge tools](#forge-tools)
  - [Forge plugins](#forge-plugins)
  - [Forge federation protocols](#forge-federation-protocols)
- [Miscellaneous projects](#miscellaneous-projects)
  - [Search](#search)
  - [Social sharing](#social-sharing)
  - [Other projects](#other-projects)
- [Reference material](#reference-material)
  - [Protocol specifications](#protocol-specifications)
  - [API documentation](#api-documentation)
- [Tutorials](#tutorials)
  - [Getting started](#getting-started)
  - [ActivityPub server-to-server (S2S)](#activitypub-server-to-server-s2s)
  - [ActivityPub client-to-server (C2S)](#activitypub-server-to-server-s2s)
  - [WebFinger](#webfinger)
  - [Security](#security)
- [Research & Development](#research-development)
  - [Datashards](#datashards)
  - [Object capabilities](#object-capabilities)
  - [Federated auth/authz](#federated-auth-authz)
  - [Content addressing](#content-addressing)
  - [Peer-to-peer networking](#peer-to-peer-networking)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Developer tools

### Frameworks

* [**Bonfire**](https://bonfirenetworks.org): An extensible framework with a big focus on customisation and flexibility. Can be used to build new federated apps while focusing on a specific use case rather than reimplementing lots of boilerplate   `AGPL-3.0, Elixir`

### Libraries

* :heavy_check_mark: [**ActivityPhp**](https://github.com/landrok/activitypub) ([site](https://landrok.github.io/activitypub), [Fedi account](https://cybre.space/@landrok)): A PHP implementation of ActivityPub protocol based upon the ActivityStreams 2.0 data format. `MIT, PHP`

* :heavy_check_mark: [**ActivityPods**](https://github.com/assemblee-virtuelle/activitypods): Adding intelligence to Solid PODs with ActivityPub (based on [SemApps](https://semapps.org). `Apache-2.0, Javascript`

* :heavy_check_mark: [**ActivityPub**](https://github.com/bonfire-networks/activity_pub): Generic library to federate an app with ActivityPub (uses a queue for incoming/outgoing activities and adapter modules for tight integration). `AGPL-3.0, Elixir`

* :heavy_check_mark: [**ActivityPub Express**](https://github.com/immers-space/activitypub-express): Modular ActivityPub implementation as Express.js middleware to easily add decentralization and federation to Node apps `-, Javascript`

* [**ActivityPub-Core**](https://github.com/michaelcpuckett/activitypub-core):  An attempt to build a spec-compliant ActivityPub core library. `MIT, Typescript`

* :heavy_check_mark: [**ActivityPub-Federation**](https://github.com/LemmyNet/activitypub-federation-rust): A high-level framework for ActivityPub federation in Rust, extracted from Lemmy. `AGPL-3.0, Rust`

* :heavy_check_mark [**ActivityPub Models**](https://github.com/activitypub-js/activitypub-models) - "ActivityPub JS/TS models with docs `TypeScript, -`

* :heavy_check_mark: [**ActivityPub HTTP Signatures**](https://gitlab.com/paulkiddle/activitypub-http-signatures) ([Fedi account](https://kith.kitchen/@paul)): A library for creating, parsing, and verifying HTTP signature headers, as per the Signing HTTP Messages draft 80 specification. `ISC, Javascript`

* :heavy_check_mark: [**ActivityPub-PHP**](https://github.com/pterotype-project/activitypub-php) ([Fedi account](https://mastodon.technology/@jdormit)): A PHP implementation of the ActivityPub protocol (used in Pterotype plugin).  `MIT, PHP`

* [**ActivityPub Starter Kit**](https://github.com/jakelazaroff/activitypub-starter-kit): A tiny, single user ActivityPub server, meant to be a starting point for your own ActivityPub projects. `MIT, Typescript`

* :heavy_check_mark: [**activityPub4j**](https://github.com/msummers/activityPub4j): W3C ActivityPub and ActivityStreams implementation in Java using Spring Boot. `Apache-2.0, Java`

* [**ActivityHub.Net**](https://github.com/Meep-Tech/ActivityPub.Net): A C# .NET Implementation of ActivityPub Entity Types. `-, C#`

* [**ActivityPub.Net**](https://github.com/judgy/ActivityPub.Net): A parser for ActivityPub. `MIT, C#`

* :heavy_check_mark: [**ActivityServe**](https://github.com/writeas/activityserve): A very light ActivityPub library in Go (used to power [pherephone](https://github.com/writeas/pherephone)) `MIT, Go`

* :heavy_check_mark: [**ActivityStreams**](https://git.asonix.dog/asonix/activitystreams) ([Fedi account](https://masto.asonix.dog/@asonix)): A set of Traits and Types that make up the ActivityStreams and ActivityPub specifications. `GPL-3.0, Rust`

* :heavy_check_mark: [**ActivityStreams**](https://github.com/OpenSocial/activitystreams): Full ActivityStreams 1.0 and 2.0 reference implementation in Java. `Apache-2.0, Java`

* [**ActivityStreams**](https://github.com/KristofferStrube/ActivityStreams): A .NET implementation of the Activity Streams vocabulary in the form of classes that can be serialized using System.Text.Json `MIT, C#`

* :heavy_check_mark: [**ActivityStreams-2**](https://github.com/gobengo/activitystreams2): ActivityStreams 2.0 library for Node.js and TypeScript. `Apache-2.0, Typescript`

* :heavy_check_mark: [**astreams**](https://github.com/MatejLach/astreams) ([Fedi account](https://social.matej-lach.me/@MatejLach)): A hand-crafted implementation of the Activity Streams 2.0 specification in Go, especially suitable for projects implementing ActivityPub. `AGPL-3.0, Go`

* :heavy_check_mark: [**atoot**](https://github.com/popura-network/atoot): Library providing an easy way to create Mastodon API applications `MIT, Python`

* [**Clovis**](https://github.com/WellFactored/clovis): [ARCHIVED] Implementation of ActivityPub in Scala. (Hobby project included here for reference only.) `AGPL-3.0, Scala`

* [**Bovine**](https://github.com/HelgeKrueger/bovine): A modular Activity Server. `MIT, Python`

* :heavy_check_mark: [**corpus-activity-streams**](https://github.com/ryanatkn/corpus-activity-streams): Activity Streams 2.0 vocabulary data and alternative docs. `Unlicense license, Typescript`

* [**DarkLink.Web.ActivityPub**](https://github.com/WiiPlayer2/DarkLink.Web.ActivityPub): A framework (kinda) to work with ActivityPub with .NET `MIT, C#`

[**Dialtone**](https://codeberg.org/rcode3/dialtone): An "un-imagined" ActivityPub service, to address emergent use cases. Multi-tenant, multi-personality, multi-ownership. `Apache-2.0 / MIT, Rust`

* :heavy_check_mark: [**Disboard**](https://github.com/OrionDevelop/Disboard): Collection of fediverse API wrapper libraries for .NET Standard 2.0. `MIT, C#`

* [**Drastic.Mastodon**](https://github.com/drasticactions/Drastic.Mastodon): A fork of Masto.NET, a C#/.NET Library for accessing Mastodon. `MIT, C#`

* :heavy_check_mark: [**Express ActivityPub**](https://github.com/dariusk/express-activitypub): A very simple reference implementation of an ActivityPub server using Express.js. `MIT, Javascript`

* :heavy_check_mark: [**Federation**](https://gitlab.com/jaywink/federation): Library to abstract social web federation protocols like ActivityPub, Diaspora and Matrix (see [docs](https://federation.readthedocs.io)) `BSD-3-clause, Python`

* [**fedi-inbox**](https://gitlab.com/paulkiddle/fedi-inbox) ([Fedi account](https://meow.social/@stokes)): Basic Fediverse Inbox. Writing software is hard and time consuming. What's the most basic practical system that lets us connect to the fediverse? `-, Javascript`

* [**fuwuqi**](https://git.exozy.me/a/fuwuqi) ([Fedi account](https://social.exozy.me/@a)): A useless C2S ActivityPub server for "extremely hardcore" ActivityPub enthusiasts. `GPL-3.0, Python`

* :heavy_check_mark: [**Golang ActivityPub**](https://github.com/go-ap) ([Fedi account](https://metalhead.club/@mariusor)): Libraries for using ActivityPub in the Go language. `MIT, Go`

* :heavy_check_mark: [**go-fed activity**](https://github.com/go-fed/activity) ([site](https://go-fed.org), [Fedi account](https://mastodon.technology/@cj)): Full ActivityStreams & ActivityPub implementation in Golang. Extensions can be easily added by design-time code generation from JSON-LD schema's (also supports [ForgeFed](https://forgefed.org/) this way, by default). `BSD-3-clause, Go`

* :heavy_check_mark: [**go-fed apcore**](https://github.com/go-fed/apcore) ([site](https://go-fed.org), [Fedi account](https://mastodon.technology/@cj)): A powerful single server ActivityPub framework for performant Fediverse applications. `AGPL-3.0, Go`

* [**go-mastodon**](https://github.com/mattn/go-mastodon): Mastodon client for Golang. `MIT, Go`

* :heavy_check_mark: [**hare-activity**](https://git.sr.ht/~torresjrjr/hare-activity) ([Fedi account](https://qoto.org/@torresjrjr)): Hare package provides a extensible implementation of the ActivityStreams 2.0 format and the ActivityPub protocols. `MPL, Hare`

* :heavy_check_mark: [**http-signature**](https://github.com/mtti/http-signature): Implementation of the HTTP Signature scheme as used in ActivityPub. `Apache-2.0, Typescript`

* [**Incognitum**](https://github.com/Joe-K-Sewell/Incognitum): A .NET Standard library for Mastodon. `MIT, C#`

* :heavy_check_mark: [**Little Boxes**](https://little-boxes.readthedocs.io): Tiny ActivityPub framework, both database and server agnostic `ISC, Python`

* :heavy_check_mark: [**Little Library**](https://github.com/Alamantus/little-library): A digital give-a-book, take-a-book library for ebooks `AGPL-3.0, Javascript`

* :heavy_check_mark: [**Mastodon.py**](https://github.com/halcy/Mastodon.py): Python wrapper for the Mastodon API. `MIT, Python`

* [**Masto.Net**](https://github.com/glacasa/Mastonet):  A C# Library for Mastodon. `MIT, C#`

* [**Mod.ActivityPub**](https://github.com/sirtwist/Mod.ActivityPub):  ActivityPub Server services for ASP.NET `-, C#`

* :heavy_check_mark: [**Pubstrate**](https://gitlab.com/dustyweb/pubstrate): ActivityStreams and ActivityPub library implementation for GNU Guile. Includes a full ActivityStreams library and most of an ActivityPub implementation. `GPL-3.0, Guile`

* :heavy_check_mark: [**Python ActivityPub**](https://github.com/dsblank/activitypub): A general ActivityPub library `MPL-2.0, Python`

* [**rdf-pub**](https://gitlab.com/linkedopenactors/rdf-pub) ([site](https://linkedopenactors.gitlab.io/rdf-pub/), [Fedi account](https://chaos.social/@naturzukunft)): An activity-pub server implementation, that is not limited to the activity-stream vocabulary, but supports RDF per se. `EUPL, Java`

* [**Sigh**](https://github.com/astro/sigh) ([Fedi account](https://c3d2.social/@astro)):  HTTP signatures for ActivityPub in Rust. `MIT, Rust`

* [**The Old Dog and Duck**](https://github.com/simon-brooke/dog-and-duck): A Clojure library designed to implement the ActivityPub protocol. `GPL-2.0, Clojure`

### Plugins

* :heavy_check_mark: [**ActivityPub for Drupal**](https://github.com/swentel/activitypub) ([Lead dev](https://realize.be/user/1)): ActivityPub integration for Drupal 8 (see also [lite version](https://www.drupal.org/project/activitypub/) at Drupal) `GPL-2.0, PHP`

* :heavy_check_mark: [**FediEmbedi**](https://codeberg.org/mediaformat/fediembedi) ([Fedi account](https://social.coop/@django)): A wordpress plugin widget to display your fediverse account timeline. `GPL-3.0, PHP`

* :heavy_check_mark: [**Hugo-ActivityStreams**](https://git.jlel.se/jlelse/hugo-activitystreams): A Hugo module for use in Hugo, to generate ActivityStreams representations of posts. ([mirror](https://codeberg.org/jlelse/hugo-activitystreams)) `MIT, Go`

* :heavy_check_mark: [**Mastodon Wordpress Autopost**](https://github.com/simonfrey/mastodon_wordpress_autopost) ([site](https://wordpress.org/plugins/autopost-to-mastodon/#description)): A Wordpress Plugin that automatically posts your new articles to Mastodon. `GPL-2.0, PHP`

* :heavy_check_mark: [**Pterotype**](https://github.com/pterotype-project/pterotype): WordPress plugin. Pterotype connects your blog to the Fediverse by giving it an ActivityPub feed `MIT, PHP`

* :heavy_check_mark: [**rustypub**](https://github.com/hachyserve/rustypub) ([site](https://hachyserve.github.io/rustypub), [Fedi account](https://hachyderm.io/@dma)): Early beginnings of ActivityPub library in Rust (started August 2022, see [announcement](https://hachyderm.io/@dma/109021342487641187)). `Apache-2.0, Rust`

* :heavy_check_mark: [**WordPress-ActivityPub**](https://github.com/pfefferle/wordpress-activitypub): ActivityPub for Wordpress `MIT, PHP`

* :heavy_check_mark: [**WordPress-OStatus**](https://github.com/pfefferle/wordpress-ostatus): An OStatus plugin for WordPress `MIT, PHP`

* :heavy_check_mark: [**XWiki Extension for ActivityPub**](https://github.com/xwiki-contrib/application-activitypub) ([site](https://extensions.xwiki.org/xwiki/bin/view/Extension/ActivityPub%20Application/), [Fedi account](https://social.weho.st/@XWiki)): An implementation of the ActivityPub protocol for XWiki (see [forum discussion](https://forum.xwiki.org/t/new-application-activitypub/6186)). `LGPL-2.1, Java`

### Relays

* :heavy_check_mark: [**ActivityRelay**](https://git.pleroma.social/pleroma/relay): A generic LitePub relay (works with all LitePub consumers and Mastodon). `AGPL-3.0, Python`

* :heavy_check_mark: [**activity-relay**](https://github.com/yukimochi/Activity-Relay): Yet another powerful customizable ActivityPub relay server written in Go `AGPL-3.0, Go`

* :heavy_check_mark: [**Hash2Pub**](https://git.orlives.de/schmittlauch/Hash2Pub) ([Fedi account](https://toot.matereal.eu/@schmittlauch)): A fully-decentralised DHT-based relay for global hashtag federation. See [White paper](https://git.orlives.de/schmittlauch/paper_hashtag_federation), [status update](https://socialhub.activitypub.rocks/t/542/19) (Nov 2020) `AGPL-3.0, Haskell`

* :heavy_check_mark: [**Pub-Relay**](https://github.com/noellabo/pub-relay) ([Fedi account](https://fedibird.com/@noellabo)): A service-type ActivityPub actor that will re-broadcast anything sent to it to anyone who subscribes to it. `AGPL-3.0, Crystal`

* :heavy_check_mark: [**Seattle relay**](https://gitlab.com/jankysolutions/social.seattle.wa.us/relay) `-, Python`

* :heavy_check_mark: [**Social relay**](https://github.com/jaywink/social-relay): Public post relay for the diaspora* federated social network protocol. `AGPL-3.0, Python`

* :heavy_check_mark: [**toot-relay**](https://github.com/DagAgren/toot-relay): Relay that forwards web push notifications to APNs, built for Toot!.app but usable for anyone. `CC0, Go`

* [**AodeRelay**](https://git.asonix.dog/asonix/relay): A simple and efficient ActivityPub relay. `AGPL-3.0, Rust`

For an overview of known relays, see [Relay List, a regularly updated, sorted list of ActivityPub relays for Mastodon or Pleroma](https://relaylist.com).

### Bridges

* :heavy_check_mark: [**BirdSiteLIVE**](https://github.com/NicolasConstant/BirdsiteLive) ([Fedi account](https://fosstodon.org/@BirdsiteLIVE)): An ethical bridge from Twitter `AGPL-3.0, C#`

* :heavy_check_mark: [**bleroma**](https://github.com/4DA/bleroma): Telegram bot for Pleroma and Mastodon. `MIT, Elixir`

* :heavy_check_mark: [**Bridgy Fed**](https://github.com/snarfed/bridgy-fed) ([site](https://fed.brid.gy), [Fedi account](https://fed.brid.gy/user/snarfed.org)): Bridges the IndieWeb to federated social networks: ActivityPub, OStatus, etc. `Public Domain, Python`

* :heavy_check_mark: [**Fediverse-Action**](https://github.com/rzr/fediverse-action): Github Action that posts to Fediverse when code is changed. `ISC License, Javascript`

* :heavy_check_mark: [**Toot-Together**](https://github.com/joschi/toot-together) ([Fedi account](https://social.tchncs.de/@commit2toot)): Github Action that posts to Mastodon from text files, enabling everyone to submit toot drafts to a project. `MIT, Javascript`

* :heavy_check_mark: [**feed2toot**](https://gitlab.com/chaica/feed2toot): Parses RSS feeds, identifies new posts and posts them on the Mastodon social network ([using the Mastodon API]((https://gitlab.com/chaica/feed2toot/issues/35#note_289027030))). `MIT, Python`

* :heavy_check_mark: [**feediverse**](https://github.com/edsu/feediverse): Send RSS/Atom feeds to Mastodon. `MIT, Python`

* :heavy_check_mark: [**gemifedi**](https://git.sr.ht/~boringcactus/gemifedi): A Gemini frontend to the fediverse (specifically, Mastodon and Pleroma instances). `AGPL-3.0, Rust`

* :heavy_check_mark: [**Instagram2Fedi**](https://github.com/Horhik/Instagram2Fedi): Script for crossposting from Instagram to Mastodon or Pixelfed. `GPL-3.0, Python`

* :heavy_check_mark: [**Kazarma**](https://gitlab.com/kazarma/kazarma/): A Matrix bridge to ActivityPub. `AGPL-3.0, Elixir`

* :heavy_check_mark: [**Libervia**](https://repos.goffi.org/sat/file/tip) ([site](https://salut-a-toi.org/), [Fedi account](https://mastodon.social/@Goffi)): An XMPP <=> ActivityPub gateway project doubled with XMPP Pubsub end-to-end encryption `AGPL-3.0, Python`

* :heavy_check_mark: [**ligh7hau5**](https://github.com/vulet/ligh7hau5): A Matrix to Fediverse / ActivityPub client / bridge. Also, some media proxying. `GPL-3.0, Javascript`

* :heavy_check_mark: [**mastodon-bot**](https://gitlab.com/yogthos/mastodon-bot): a bot for mirroring Twitter / Tumblr accounts and RSS feeds on Mastodon `MIT, Clojure`

* :heavy_check_mark: [**mastodon-twitter-poster**](https://github.com/renatolond/mastodon-twitter-poster) ([site](https://crossposter.masto.donte.com.br/), [Fedi account](https://masto.donte.com.br/@crossposter)): Crossposter to post statuses between Mastodon and Twitter. `AGPL-3.0, Ruby`

* :heavy_check_mark: [**mastotuit**](https://git.mastodont.cat/spla/mastotuit) ([Fedi account](https://soc.catala.digital/@spla)): Publishes to Twitter all your Mastodon public text only posts but also your posts with images, with videos and even your polls. `GPL-3.0, Python`

* :heavy_check_mark: [**Moa**](https://gitlab.com/fedstoa/moa) ([site](https://moa.party/)): A Mastodon, Twitter, and Instagram Cross-poster. `MIT, Python`

* :heavy_check_mark: [**MXToot**](https://github.com/ma1uta/mxtoot) ([Fedi account](https://mastodon.social/@ma1uta)): A Matrix <--> Mastodon bot written in Java. `Apache-2.0, Java`

* :heavy_check_mark: [**Nautilus**](https://github.com/aaronpk/Nautilus): A standalone service to deliver posts from your own website to ActivityPub followers. `Apache-2.0, PHP`

* :heavy_check_mark: [**PeerTube (on Matrix) Search**](https://github.com/vranki/hemppa#peertube-search): Search PeerTube via Matrix, using Sepia Search API to search on all participating public PeerTube instances. You can also select any single instance. It's implemented as a module for Hemppa the bot. `GPL-3.0, Python`

* :heavy_check_mark: [**pleroma-bot**](https://gitea.robertoszek.xyz/robertoszek/pleroma-bot): Mirrors Twitter accounts to Fediverse, or migrates Twitter account to Fediverse using a Twitter archive. Supports Mastodon, Pleroma and Misskey. `MIT, Python`

* :heavy_check_mark: [**rss-bot**](https://alexschroeder.ch/cgit/rss-bot/) ([Fedi account](https://octodon.social/@kensanata)): Post updates from an RSS feed to Mastodon. `GPL-3.0, Python`

* :heavy_check_mark: [**RSS-to-ActivityPub Converter**](https://github.com/dariusk/rss-to-activitypub): Convert any RSS feed to an ActivityPub actor that can be followed by users on ActivityPub-compliant social networks like Mastodon. `MIT, Javascript`

* :heavy_check_mark:  [**RSS-Bridge ActivityPub Bridge**](https://github.com/RSS-Bridge/rss-bridge) ([site](https://rssbridge.bus-hit.me/#bridge-Mastodon), [Fedi account](https://ieji.de/@austin)): Generates RSS and Atom feeds for websites that don't have one. Supports Mastodon, Pleroma and Misskey, among others. `Unlicense, PHP`

* :heavy_check_mark:[**Simplebot-Mastodon**](https://github.com/simplebot-org/simplebot_mastodon): A Mastodon - DeltaChat bridge plugin for SimpleBot, [list of bot instances](https://simplebot-org.github.io/simplebot-instances) `MPL-2.0, Python`

* :heavy_check_mark: [**Twitter Hamachpil**](https://gitlab.com/hamachpil/twitter_hamachpil) ([Fedi account](https://emacsen.net/@emacsen)): Bot to grab tweets from Twitter and post them to respective accounts on a Mastodon instance. `Apache-2.0, Python`

* :heavy_check_mark: [**YouTube2PeerTube**](https://github.com/mister-monster/YouTube2PeerTube): A bot that mirrors YouTube channels to PeerTube channels as videos are released in a YouTube channel. `AGPL-3.0, Python`

### Utilities

* [**ActivityPubBotDotNet**](https://github.com/KristofferStrube/ActivityPubBotDotNet) ([Fedi account](https://hachyderm.io/@KristofferStrube)): An implementation of a ActivityPub bot that can communicate with Mastodon servers. `MIT, C#`

* [**ActivityPubSchema**](https://github.com/redaktor/ActivityPubSchema): JSON Schema definition of the ActivityStreams and ActivityPub specifications. `MIT, Javascript`

* :heavy_check_mark: [**activity-streams-validator**](https://github.com/yuforium/activity-streams-validator) ([Fedi account](https://mastodon.social/@cpmoser)): Activity Streams validation for Typescript. `MIT, Typescript`

* [**AP follow**](https://github.com/mwt/apfollow) - A remote follow tool to share links and buttons that allow people to follow you from their own ActivityPub instance `BSD-2-clause, PHP`

* :heavy_check_mark: [**bridge**](https://source.joinmastodon.org/mastodon/bridge): A simple web app that helps you find your Twitter friends on the federated Mastodon network. It is also an example of how the Mastodon API can be used and the federated OAuth authorization flow. `AGPL-3.0, Ruby`

* :heavy_check_mark: [**dda-masto-embed**](https://github.com/DomainDrivenArchitecture/dda-masto-embed) ([Fedi account](https://social.meissa-gmbh.de/@team)): Embeds mastodon timline into a html page. Uses JS, no intermediate server required. `Apache-2.0, ClojureScript`

* [**f2ap**](https://github.com/Deuchnord/f2ap): A web application that uses the RSS/Atom feed of your website to expose it on the Fediverse through ActivityPub. `AGPL-3.0, Python`

* [**FediAct**](https://github.com/Lartsch/FediAct): Chrome/Firefox extension that simplifies interactions on other Mastodon instances than your own. `MIT, Javascript`

* [**fediblock-importer**](https://github.com/irubnich/fediblock-importer): Import domain blocks to your Mastodon v4+ instance. `GPL-3.0, Python`

* :heavy_check_mark: [**Fedifinder**](https://github.com/lucahammer/fedifinder) - Find fediverse addresses in the profiles of your Twitter followings `MIT, Javascript`

* :heavy_check_mark: [**FediHealth**](https://git.feneas.org/buttle/fedihealth): Software to help you define federation policies for your node. `AGPL-3.0, Python`

* [**fediverse.space**](https://gitlab.com/fediverse.space/fediverse.space) ([Fedi account](https://mastodon.social/@fediversespace), [site](https://fediverse.space/)): A tool to visualize instances in the fediverse. `AGPL-3.0, Elixir`

* :heavy_check_mark: [**Fediverse Stats**](https://gitlab.com/spla/fediverse) ([Fedi account](https://mastodont.cat/@fediverse)): Collects maximum number of alive fediverse's servers and then query their API to obtain their registered users. `GPL-3.0, Python`

* [**Followgraph**](https://github.com/gabipurcaru/followgraph) ([site](https://followgraph.vercel.app/), [Fedi account](https://mastodon.online/@gabipurcaru)): A tol to expand your connection graph and find new people to follow. (Unlicensed, see [open issue](https://github.com/gabipurcaru/followgraph/issues/9)) `-, Typescript`

* :heavy_check_mark: [**forget**](https://github.com/codl/forget) ([site](https://forget.codl.fr/), [Fedi account](https://chitter.xyz/@codl)): Continuous post deletion for Mastodon and Twitter (if you happen to use that). `ISC License, Python`

* :heavy_check_mark: [**hunter2**](https://github.com/Flockingbird/hunter2) ([site](https://search.flockingbird.social/), [Fedi account](https://botsin.space/@hunter2)): A job hunt bot that indexes jobs and candidates from the Fediverse. `MIT, Rust`

* [**Granary**](https://github.com/snarfed/granary) ([site](https://granary.io/)): The social web translator. Fetches and converts data between social networks, HTML and JSON with microformats2, ActivityStreams, Atom, JSON Feed, and more. `Public domain, Python`

* :heavy_check_mark: [**MaPleFeed**](https://github.com/bihlink/maplefeed) ([site](https://maplefeed.bihlink.com/), [Fedi account](https://abid.cc/users/abid)): Embed Mastodon or Pleroma profile feeds on any web page. `MIT, Javascript`

* :heavy_check_mark: [**MastoBot**](https://gitlab.com/eroosenmaallen/mastobot) ([Fedi account](https://roosenmaallen.com/author/admin/)): NodeJS Mastodon client library with an eye to making bot development fun & easy. `ISC License, Javascript`

* :heavy_check_mark: [**mastodon-backup**](https://github.com/kensanata/mastodon-backup) ([Fedi account](https://octodon.social/@kensanata)): Archive your statuses, favorites and media using the Mastodon API. `GPL-3.0, Python`

* :heavy_check_mark: [**Mastodon Backup Resore**](https://codeberg.org/DecaTec/Mastodon-Backup-Restore): Bash scripts for backup / restore of Mastodon. `MIT, Shell`

* :heavy_check_mark: [**mastodon_bulk_find_follow**](https://gitlab.com/ozten/mastodon_bulk_find_follow) ([site](https://commotionmade.com/)): Bulk find and follow to make it easy to migrate from Twitter to Mastodon. `BSD-3-Clause, Javascript`

* [**mastodon_digest**](https://github.com/hodgesmr/mastodon_digest) ([Fedi account](https://mastodon.social/@MattHodges)): A script that aggregates recent popular posts from your Mastodon timeline. `BSD-3-Clause, Python`

* [**MastodonKit**](https://github.com/MastodonKit/MastodonKit) ([site](https://mastodonkit.github.io/MastodonKit/)): A Swift Framework that wraps Mastodon's API.  `MIT, Swift`

* [**mastodon_recommender**](https://github.com/HelgeKrueger/mastodon_recommender): Script that recommends people for you to follow on Mastodon based on your own account. `-, Python`

* :heavy_check_mark: [**Masto Follow**](https://gitlab.com/eroosenmaallen/masto-follow) ([site](https://eroosenmaallen.gitlab.io/masto-follow/), [Fedi account](https://tech.lgbt/@silvermoon82)): Simple widget to support “Follow Me on Mastodon” buttons `ISC License, Javascript`

* :heavy_check_mark: [**MastoMods**](https://github.com/trwnh/mastomods) ([Fedi account](https://mastodon.social/@trwnh)): CSS tweaks and custom themes for Mastodon. `Unlicense, CSS`

* :heavy_check_mark: [**Masto Thread Renderer**](https://github.com/vrutkovs/masto-thread-renderer) ([site](https://thread.vrutkovs.eu), [Fedi account](https://social.vrutkovs.eu/users/vadim)): Display a Mastodon thread (only author toots, no replies) as a web page or Markdown for reading. `Apache-2.0, Rust`

* :heavy_check_mark: [**Mastotool**](https://github.com/muesli/mastotool): A collection of tools to work with your Mastodon account; displays account statistics and lets you search your toots. `MIT, Go`

* [**Minoru's Fediverse Crawler**](https://github.com/Minoru/minoru-fediverse-crawler) ([site](https://nodes.fediverse.party/), [Fedi account](https://functional.cafe/@minoru)): Crawls the Fediverse and produces a list of instances that are alive. `AGPL-3.0, Rust`

* :heavy_check_mark: [**OCR Bot**](https://github.com/Lynnesbian/OCRbot/) ([Fedi account](https://fedi.lynnesbian.space/@lynnesbian)): An OCR (Optical Character Recognition) bot for Mastodon (and compatible) instances `AGPL-3.0, Python`

* :heavy_check_mark: [**Pherephone**](https://github.com/writeas/pherephone): An ActivityPub server that reblogs all the statuses of certain actors. You set it up to follow a few accounts and it announces everything they post. `AGPL-3.0, Go`

* :heavy_check_mark: [**Shareon**](https://codeberg.org/kytta/shareon) ([site](https://shareon.js.org/), [Fedi account](https://fosstodon.org/@kytta)): Lightweight, stylish, and ethical share buttons. `MIT, Javascript`

* [**Steampipe Mastodon Plugin**](https://github.com/turbot/steampipe-plugin-mastodon): Use SQL to instantly query Mastodon timelines and more. Open source CLI to Steampipe SaaS. No DB required. `Apache-2.0, Go`

* [**Steampipe Mastodon Insights Mod**](https://github.com/turbot/steampipe-mod-mastodon-insights): View Mastodon timelines, search hashtags, find interesting people, and check server stats. `Apache-2.0, SourcePawn`

* :heavy_check_mark: [**tags-pub**](https://gitlab.com/evanp/tags-pub): Provides hashtag objects on the ActivityPub network. `Apache-2.0, Javascript`

* :heavy_check_mark: [**toot**](https://codeberg.org/kytta/toot) ([site](https://toot.kytta.dev/), [Fedi account](https://fosstodon.org/@kytta)): Cross-instance share page for Mastodon `AGPL-3.0, Javascript`

* :heavy_check_mark: [**Twitodon**](https://github.com/diddledani/twitodon) ([site](https://twitodon.com)) - Twitter to Mastodon account mapping service to aid migration away from Twitter without losing all your followed friends `- , Javascript`

* :heavy_check_mark: [**yt2pt**](https://github.com/buoyantair/yt2pt): A simple set of scripts to quickly import your youtube channel to Peertube. `MIT, Javascript`

* :heavy_check_mark: [**Moderator Alerts**](https://gitlab.com/stemid/mastodon-moderator-alerts) ([Fedi account](https://mastodon.se/@stemid)): Mastodon moderation alerts using Pushover.net. `-, Python`

### Testing

* :heavy_check_mark: [**activitypub-mock**](https://gitlab.com/evanp/activitypub-mock): A mock ActivityPub server to use in testing code `Apache-2.0, Javascript`

* :heavy_check_mark: [**APDebug**](https://codeberg.org/samuelroland/apdebug) ([Fedi account](https://fosstodon.org/@samuelroland)): A set of tools to debug ActivityPub requests. You can inspect requests, see the flow in real time, run arbitrary requests with HTTP signature handled for you. `AGPL-3.0, PHP`

* [**dfk-ap**](https://glitch.com/edit/#!/dfk-ap?path=README.md%3A1%3A0) ([site](https://tinysubversions.com/notes/activitypub-tool/)): A small ActivityPub debugging server on Glitch `MIT, Javascript`

* :heavy_check_mark: [**FediDB**](https://github.com/fedidb/fedidb-ce) ([site](https://fedidb.org/about), [Fedi account](https://mastodon.social/@dansup)): A suite of tools for AP devs to help make it easier to test and validate your implementation with existing implementations like Mastodon, PeerTube, Pixelfed and Pleroma `AGPL-3.0, PHP`

* :heavy_check_mark: [**Test Suite**](https://github.com/go-fed/testsuite): An unofficial partially-automated ActivityPub test suite `AGPL-3.0, Go`

## Forge federation

This category is dedicated to an important new direction on the Fediverse, where Software Development itself becomes federated. Currently the focus is on projects and protocols that allow for the federation of code forges (such as Gitea and Gitlab).

### Forge libraries

* :heavy_check_mark: [**gofff**](https://lab.forgefriends.org/friendlyforgeformat/gofff) ([Fedi account](https://mastodon.online/@dachary)): Reference implementation of Friendly Forge Format in Golang. `AGPL-3.0, Go`

* :heavy_check_mark: [**pyfff**](https://lab.forgefriends.org/friendlyforgeformat/pyfff) ([Fedi account](https://mastodon.online/@dachary)): Reference implementation of Friendly Forge Format in Python. `AGPL-3.0, Python`

### Forge tools

* :heavy_check_mark: [**ForgeFlux StarChart**](https://github.com/forgeflux-org/starchart): Software forge spider. Crawler that maps and advertises self-hosted code forge instances. `AGPL-3.0, Rust`

* :heavy_check_mark: [**giteacat**](https://git.mastodont.cat/spla/giteacat): Python script that allows sign up to a Gitea instance to all local users of a Mastodon server. `-, Python`

### Forge plugins

* :heavy_check_mark: [**ForgeFed extension for Pagure**](https://pagure.io/pagure-forgefed): An extension for the Pagure forge that adds ForgeFed federation to Pagure instances. `GPL-2.0, Python`

### Forge federation protocols

* :heavy_check_mark: [**ForgeFed**](https://codeberg.org/ForgeFed/ForgeFed) ([site](https://forgefed.org/), [Fedi account](https://floss.social/@forgefed)): A set of extensions to ActivityPub for federation between code forges (Reference implementation is [Vervis](https://vervis.peers.community/s/fr33domlover/p/vervis)). `CC0-1.0`

* :heavy_check_mark: [**Friendly Forge Format**](https://lab.forgefriends.org/friendlyforgeformat) ([Fedi account](https://mastodon.online/@dachary)): An Open File Format for storing the information from a forge. `-`

## Miscellaneous projects

This category is for any code project related to the Fediverse. They need not be directly development related.

### Search

* [**Sepia Search**](https://framagit.org/framasoft/peertube/search-index/) ([site](https://sepiasearch.org/)): A search engine of PeerTube videos and channels Developed by Framasoft `AGPL-3.0, Typescript`

### Social sharing

* :heavy_check_mark: [**Share Buttons**](https://git.fsfe.org/FSFE/share-buttons): Share buttons that support dynamic input of Fediverse URLs and require no Javascript. `AGPL-3,0, PHP`

* :heavy_check_mark: [**Fedishare**](https://gitlab.com/mugcake/fedishare): Firefox toolbar extension to share the current browser tab on the Fediverse `GPL-3.0, Typescript`

### Other projects

* [**Mastodon Simplified Federation**](https://github.com/rugk/mastodon-simplified-federation): FireFox browser plugin, that simplifies following and interacting with remote users on other Mastodon instances. `ISC, Javascript`

## Reference material

### Protocol specifications

* :heavy_check_mark: [**LitePub**](https://github.com/litepub/litepub) ([Fedi account](https://pleroma.site/users/kaniini)): A set of extensions to AP, being developed by devs from Pleroma and Mastodon (status: for the most part litepub group folded back into SocialCG, see: [issue](https://github.com/litepub/litepub/issues/6))

* [**NodeInfo**](https://github.com/jhass/nodeinfo): Defines a standardized way to expose metadata about an installation of a distributed social network. `CC0`

* [**Podcasting**](https://github.com/Podcastindex-org/activitypub-spec-work): Workspace for defining ActivityPub and ActivityStream extensions for Podcasting with the intention to define a W3C Recommendation for them.

* :heavy_check_mark: [**SciFed**](https://synalp.frama.io/olki/scifed/) (Fedi accounts: [@rigelk](https://olki-social.loria.fr/@rigelk), [@cerisara](https://mastodon.etalab.gouv.fr/@cerisara)): A specification standard (Draft) for federation of scientific activities and content using ActivityPub, developed as part of the [OLKi](https://olki.loria.fr/) project.

* :ghost: [**NodeInfo2**](https://git.feneas.org/jaywink/nodeinfo2): An effort to create a standardized way of exposing metadata about a server. Helps expose ownership and organization details, usage statistics and protocol capabilities. (disappeared when [feneas shut down](https://socialhub.activitypub.rocks/t/important-feneas-federated-networks-association-will-shut-down/2135)) `CC0-1.0`

### API documentation

## Tutorials

### Getting started

- [Guide for new ActivityPub Implementers](https://socialhub.activitypub.rocks/t/guide-for-new-activitypub-implementers/479): A crowd-sourced wiki maintained by SocialHub community with a ton of information and links.

- [ActivityPub for Podcast Client Apps](https://minipub.dev/info/activitypub-for-podcast-apps/): Overview on how to implement cross-app comments for podcasts.

### ActivityPub server-to-server (S2S)

- [ActivityPub Explained](https://github.com/boyter/activitypub): Example payloads and Diagrams of common activities

### ActivityPub client-to-server (C2S)

### WebFinger

### Security

### NodeInfo

## Research & Development

### Datashards

### Object capabilities

### Federated auth/authz

* [**Activity Anchors**](https://trustbloc.github.io/activityanchors/) ([github](https://github.com/trustbloc/activityanchors)): An anchoring system for a fediverse of interconnected nodes and witnesses.

* [**The did:orb Method**](https://trustbloc.github.io/did-method-orb/) ([github](https://github.com/trustbloc/did-method-orb)): A DID Method for a fediverse of interconnected nodes and witnesses.

### Content addressing

* [**Content-addressible RDF**](https://openengiadina.net/papers/content-addressable-rdf.html): A scheme based on RDF allowing for data to be referred to by an identifier determined by the data itself (written by @pukkamustard of [openEngiadina](https://openengiadina.net), specification now part of [DREAM](https://dream.public.cat/pub/dream-data-spec))

* [**Encoding for Robust Immutable Storage** (ERIS)](https://inqlab.net/projects/eris/): ERIS is an encoding of arbitrary content into a set of uniformly sized, encrypted and content-addressed blocks as well as a short identifier (a URN) (written by @pukkamustard of [openEngiadina](https://openengiadina.net), specification now part of [DREAM](https://dream.public.cat/pub/dream-data-spec)).

### Distributed Mutable Containers

* [**Distributed Mutable Containers** (DMC)](https://inqlab.net/projects/dmc/): Distributed data structures that can hold references to content while allowing replicas of the data structures to diverge and merge without conflict (developed by [DREAM](https://dream.public.cat/pub/dream-data-spec))

### Peer-to-peer networking

## Maintainers

If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/fediverse/delightful-activitypub-development/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@circlebuilder`](https://codeberg.org/circlebuilder)
- [`@lostinlight`](https://codeberg.org/lostinlight)

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](LICENSE)
