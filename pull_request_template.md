---

name: "Addition"
about: "This template is for testing!"
title: "[Addition] SoftwareName"
ref: "main, Pull_Request_Template"
labels:

- Addition

---

[SoftwareName](https://example.io/repo) ([site](https://example.com), [Fedi account](https://example.social)): Short description of the software. LICENSE, Programming Language